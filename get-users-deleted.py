import argparse
import json
import requests
from datetime import datetime, timedelta
import re
import csv

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class Parameters:
    kibana_url = None
    kibana_user = None
    kibana_password = None
    nginx_index_pattern = None
    pmd_index_pattern = None
    pmd_url = None
    pmd_user = None
    pmd_password = None
    pmd_token = None
    debug = False
    datetime_from = None
    datetime_to = None
    output = None

PMD_CACHE = {}


def get_users_deletions(params: Parameters):
    method = 'POST'
    uri = 'api/console/proxy?path=%2F{}%2F_search&method=GET'.format(params.nginx_index_pattern)

    request_data = '{{"size":1000,"query":{{"bool":{{"filter":[{{"multi_match":{{"type":"phrase","query":"DELETE /api/users","lenient":true}}}},{{"range":{{"@timestamp":{{"gte":"{}","lte":"{}","format":"strict_date_optional_time"}}}}}}]}}}},"sort":[{{"@timestamp":{{"order":"desc","unmapped_type":"boolean"}}}}]}}'
    request_data = request_data.format(params.datetime_from.isoformat(), params.datetime_to.isoformat())

    response_data = send_kibana_request(params, method, uri, request_data)

    user_deletions = []
    for result in response_data['hits']['hits']:
        d = {}        
        response_access = result['_source']['nginx']['access']
        d['user_id'] =  response_access['url'].split('/api/users/')[1]
        d['remote_ip'] = response_access['remote_ip']
        d['delete_timestamp'] = datetime.fromisoformat(result['_source']['@timestamp'])
        r = re.search(r"[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}", result['_source']['message'])
        d['delete_correlation_id'] = r.group(0)
        
        user_deletions.append(d)

    return user_deletions

def get_login_event_of_deleter(params: Parameters, user_deletion):
    method = 'POST'
    uri = 'api/console/proxy?path=%2F{}%2F_search&method=GET'.format(params.nginx_index_pattern)
    datetime_from = (user_deletion['delete_timestamp'] - timedelta(hours=4)).isoformat()
    datetime_to = user_deletion['delete_timestamp'].isoformat()
    request_data = '{{"query":{{"bool":{{"must":[],"filter":[{{"bool":{{"filter":[{{"multi_match":{{"type":"phrase","query":"{}","lenient":true}}}},{{"multi_match":{{"type":"phrase","query":"api/login","lenient":true}}}}]}}}},{{"range":{{"@timestamp":{{"gte":"{}","lte":"{}","format":"strict_date_optional_time"}}}}}}]}}}},"sort":[{{"@timestamp":{{"order":"desc","unmapped_type":"boolean"}}}}]}}'
    request_data = request_data.format(user_deletion['remote_ip'], datetime_from, datetime_to)

    response_data = send_kibana_request(params, method, uri, request_data)
    if response_data['hits']['total']['value'] >=0:
        result = response_data['hits']['hits'][0]
        user_deletion['login_timestamp'] = datetime.fromisoformat(result['_source']['@timestamp'])
        r = re.search(r"[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}", result['_source']['message'])
        user_deletion['login_correlation_id'] = r.group(0)
    else:
        print('{}[ERROR] No nginx login event found for IP {} between {} and {} for the deletion of {} at {}'.format(bcolors.WARNING,
            user_deletion['remote_ip'], 
            datetime_from,
            datetime_to,
            user_deletion['user_id'],
            user_deletion['delete_timestamp']
            ))


    return user_deletion
    
def get_login_details_of_deleter(params: Parameters, user_deletion):
    method = 'POST'
    uri = 'api/console/proxy?path=%2F{}%2F_search&method=GET'.format(params.pmd_index_pattern)
    datetime_from = (user_deletion['login_timestamp'] - timedelta(minutes=1)).isoformat()
    datetime_to = (user_deletion['login_timestamp'] + timedelta(minutes=1)).isoformat()
    request_data = '{{"query":{{"bool":{{"filter":[{{"bool":{{"filter":[{{"multi_match":{{"type":"phrase","query":"{}","lenient":true}}}},{{"multi_match":{{"type":"phrase","query":"pmd_private-users","lenient":true}}}}]}}}},{{"match_phrase":{{"kubernetes.pod.labels.app":"users"}}}},{{"range":{{"@timestamp":{{"gte":"{}","lte":"{}","format":"strict_date_optional_time"}}}}}}]}}}}}}'
    request_data = request_data.format(user_deletion['login_correlation_id'], datetime_from, datetime_to)

    response_data = send_kibana_request(params, method, uri, request_data)
    if response_data['hits']['total']['value'] == 1:
        message = response_data['hits']['hits'][0]['_source']['message']
        login_event_json = message.split('with key "user" : ')[1]

        login_event = json.loads(login_event_json)
        user_deletion['deleter_login'] = login_event['current']['pmd']['login']
        user_deletion['deleter_organization_id'] = login_event['current']['organizationId']
        user_deletion['deleter_profile_id'] = login_event['current']['profileId']
        
    else:
        print('{}[ERROR] {} results for the login detail on correlationId {} between {} and {} - was expecting only 1'.format(bcolors.WARNING,
            response_data['hits']['total']['value'], 
            user_deletion['login_correlation_id'],
            datetime_from,
            datetime_to            
            ))


    return user_deletion

def get_pmd_user(params: Parameters, userId):
    if userId in PMD_CACHE:
        user_result = PMD_CACHE[userId]
    else:
        user_result = send_pmd_http_request(params, 'GET', 'users/{}'.format(userId))
        PMD_CACHE[userId] = user_result
    return user_result

def get_pmd_organization(params: Parameters, organizationId):
    if organizationId in PMD_CACHE:
        orga_result = PMD_CACHE[organizationId]
    else:
        orga_result = send_pmd_http_request(params, 'GET', 'organizations/{}'.format(organizationId))
        PMD_CACHE[organizationId] = orga_result
    return orga_result
    

def get_pmd_details(params: Parameters, user_deletion):
    user_result = get_pmd_user(params, user_deletion['user_id'])

    if 'device' in user_result:
        user_deletion['deleted_user_device_login'] = user_result['device']['login']
        user_deletion['deleted_user_device_current_status'] = user_result['device']['status']
    else:
        user_deletion['deleted_user_device_login'] = ''
        user_deletion['deleted_user_device_current_status'] = ''


    if 'pmd' in user_result:
        user_deletion['deleted_user_pmd_login'] = user_result['pmd']['login']
        user_deletion['deleted_user_pmd_current_status'] = user_result['pmd']['status']
    else:
        user_deletion['deleted_user_pmd_login'] = ''
        user_deletion['deleted_user_pmd_current_status'] = ''

    user_deletion['deleted_user_organization_id'] = user_result['organizationId']
    user_deletion['deleted_user_profile_id'] = user_result['profileId']
    


    deleted_user_organization = get_pmd_organization(params, user_deletion['deleted_user_organization_id'])
    user_deletion['deleted_user_organization_type'] = deleted_user_organization['organizationType']
    user_deletion['deleted_user_organization_name'] = deleted_user_organization['name']
    deleter_organization = get_pmd_organization(params, user_deletion['deleter_organization_id'])
    user_deletion['deleter_organization_type'] = deleter_organization['organizationType']
    user_deletion['deleter_organization_name'] = deleter_organization['name']

    
def send_kibana_request(params: Parameters, method, relative_path, request_data):
    uri = '{}/{}'.format(params.kibana_url, relative_path)
    headers = {
        'content-type': 'application/json',
        'accept': 'application/json',
        'Kbn-Version': '7.6.0'
    }
  
    if params.debug:
        print(bcolors.OKGREEN + '[DEBUG] ---  Request  ---')
        print(bcolors.OKGREEN + '[DEBUG] {} {}'.format(method, uri))
        if request_data:
            print(bcolors.OKGREEN + '[DEBUG] {}'.format(request_data))

    response = requests.request(method, uri, auth=(params.kibana_user, params.kibana_password), headers=headers, data=request_data)

    if params.debug:
        print(bcolors.OKGREEN + '[DEBUG] ---  Response ---')
        print(bcolors.OKGREEN + '[DEBUG] {}'.format(response.status_code))
        print(bcolors.OKGREEN + '[DEBUG] {}'.format(response.text))
        print(bcolors.OKGREEN + '[DEBUG] {}'.format(response.headers))

    response.raise_for_status()

    if 'application/json' in response.headers.get('content-type'):
        return response.json()
    else:
        return response.text.replace('"', '')
    
def send_pmd_http_request(parameters: Parameters, method, relative_url, jsonData=None):
    url = '{}/{}'.format(parameters.pmd_url, relative_url)

    headers = {
        'content-type': 'application/json',
        'accept': 'application/json'
    }

    if parameters.pmd_token:
        headers['authorization'] = 'Bearer {}'.format(parameters.pmd_token)
    
    if parameters.debug:
        print(bcolors.OKGREEN + '[DEBUG] ---  Request  ---')
        print(bcolors.OKGREEN + '[DEBUG] {} {}'.format(method, url))
        if jsonData:
            print(bcolors.OKGREEN + '[DEBUG] {}'.format(jsonData))

    response = requests.request(method, url, headers=headers, data=jsonData)

    if parameters.debug:
        print(bcolors.OKGREEN + '[DEBUG] ---  Response ---')
        print(bcolors.OKGREEN + '[DEBUG] {}'.format(response.status_code))
        print(bcolors.OKGREEN + '[DEBUG] {}'.format(response.text))
        print(bcolors.OKGREEN + '[DEBUG] {}'.format(response.headers))
        
    response.raise_for_status()

    if 'application/json' in response.headers.get('content-type'):
        return response.json()
    else:
        return response.text.replace('"', '')

def get_token(env_params: Parameters):
    return send_pmd_http_request(env_params, 'POST', 'login', json.dumps({'login': env_params.pmd_user, 'password': env_params.pmd_password}))['token']
        

def extract_params():    
    parameters = Parameters()

    parser = argparse.ArgumentParser()
    parser.add_argument('--kibana-password', help='Kibana password', type=str, required=True)
    parser.add_argument('--pmd-login', help='PMD login', type=str, required=True)
    parser.add_argument('--pmd-password', help='PMD password', type=str, required=True)
    parser.add_argument('--x-last-days', help='Number of days from now to lookup for users deletion (default is 7 days)', type=int, default=7)
    parser.add_argument('--date-from', help='Used to specify the exact date to lookup for users deletion (to use with --date-to)', type=str)
    parser.add_argument('--date-to', help='Used to specify the exact date to lookup for users deletion (to use with --date-from)', type=str)
    parser.add_argument('-o', '--output-files-pattern', help='.csv files will be created according to this pattern (default is user_deletions)', type=str, default='user_deletions')
    parser.add_argument('-d', '--debug', help='Enable debug logs in stdout', action='store_true')

    args = parser.parse_args()
    parameters.kibana_password = args.kibana_password
    parameters.pmd_password = args.pmd_password
    parameters.pmd_user = args.pmd_login
    parameters.debug = args.debug
    parameters.output = args.output_files_pattern
    if args.date_from and args.date_to:
        parameters.datetime_from = datetime.date.fromisoformat(args.date_from)
        parameters.datetime_to = datetime.date.fromisoformat(args.date_to)
    else:
        parameters.datetime_from = (datetime.now() - timedelta(days=args.x_last_days))
        parameters.datetime_to = datetime.now()
    

    with open('env_params.json') as json_file:
        json_data = json.load(json_file)
        parameters.kibana_url = json_data['kibanaUrl']
        parameters.kibana_user = json_data['kibanaUser']
        parameters.nginx_index_pattern = json_data['nginxIndexPattern']
        parameters.pmd_index_pattern = json_data['pmdIndexPattern']
        parameters.pmd_url = json_data['pmdApiUrl']
        parameters.pmd_token = get_token(parameters)
    
    return parameters

def export_to_csv(params: Parameters, users_deletions):
    if len(users_deletions) == 0:
        return;

    csv_results = []
    headers = [
        'Deleter login timestamp',
        'Deleter IP',
        'Deleter login',
        'Deleter profile id',
        'Deleter organization Name',

        'User deletion timestamp',
        'Deleted user device login',
        'Deleted user device current status',
        'Deleted user PMD login',
        'Deleted user PMD current status',
        'Deleted user profile id',
        'Deleted user organization Name',
        ]
    for ud in users_deletions:
        csv_result = {}
        csv_result[headers[0]] = ud['login_timestamp']
        csv_result[headers[1]] = ud['remote_ip']
        csv_result[headers[2]] = ud['deleter_login']
        csv_result[headers[3]] = ud['deleter_profile_id']
        csv_result[headers[4]] = ud['deleter_organization_name']     

        csv_result[headers[5]] = ud['delete_timestamp']
        csv_result[headers[6]] = ud['deleted_user_device_login']
        csv_result[headers[7]] = ud['deleted_user_device_current_status']
        csv_result[headers[8]] = ud['deleted_user_pmd_login']
        csv_result[headers[9]] = ud['deleted_user_pmd_current_status']
        csv_result[headers[10]] = ud['deleted_user_profile_id']
        csv_result[headers[11]] = ud['deleted_user_organization_name']

        csv_results.append(csv_result)

    with open('{}.csv'.format(params.output), 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=headers, )
        writer.writeheader()
        writer.writerows(csv_results)

    with open('{}_details.csv'.format(params.output), 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=users_deletions[0].keys(), )
        writer.writeheader()
        writer.writerows(users_deletions)


def main():
    parameters = extract_params()
    users_deletions = get_users_deletions(parameters)

    print('{}[INFO] {} user deletions found during the period from {} to {} - fetching all the details'.format(
        bcolors.OKBLUE,
        len(users_deletions),
        parameters.datetime_from.strftime("%Y-%m-%d %H:%M:%S"),
        parameters.datetime_to.strftime("%Y-%m-%d %H:%M:%S")), flush=True)

    for ud in users_deletions:
        get_login_event_of_deleter(parameters, ud)
        get_login_details_of_deleter(parameters, ud)
        get_pmd_details(parameters, ud)

    export_to_csv(parameters, users_deletions)
    
    

main()


