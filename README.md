# 20230921-getusersdeleted

Sample of command to run to get the last users deleted in the past 1 day

`py get-users-deleted.py --kibana-password $KIBANA_PWD --pmd-login $PMD_LOGIN --pmd-password $PMD_PWD --x-last-days 1`

Check also params.json to specify env variables (by default USCR)

Info retrieved in final csv :
- Deleter login timestamp
- Deleter login correlation id
- Deleter IP
- Deleter login
- Deleter profile id
- Deleter  organization id
- Deleter organization name
- User deletion timestamp
- User deletion correlation id
- Deleted user id
- Deleted user device login
- Deleted user device current status
- Deleted user PMD login
- Deleted user PMD current status
- Deleted user profile id
- Deleted user organization id
- Deleted user organization name
